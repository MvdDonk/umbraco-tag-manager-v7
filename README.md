# Umbraco Tag Manager

A package to manage tags in the Umbraco back-office.

http://www.yoyocms.co.nz/umbraco-work/tag-manager-package/


## Getting Started

### Installation

> *Note:* Tag Manager has been developed against **Umbraco v7.0.4** and will support that version and above.

Tag Manager can be installed from the Our Umbraco package repository:

#### Our Umbraco package repository

To install from Our Umbraco, please download the package from:

> [https://our.umbraco.org/projects/backoffice-extensions/tag-manager](https://our.umbraco.org/projects/backoffice-extensions/tag-manager) 


---

## Contributing to this project

Anyone and everyone is welcome to contribute.

Please start ask any questions on the [Tag Manager Forum](https://our.umbraco.org/projects/backoffice-extensions/tag-manager/tag-manager/) on Our Umbraco


### Special thanks

* ???


## License

Copyright &copy; 2011 [Nigel Wilson, yoyocms](http://www.yoyocms.co.nz/)

Licensed under the [MIT License](LICENSE.md)

