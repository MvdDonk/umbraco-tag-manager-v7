﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Umbraco.Core.Persistence;
using Umbraco.Web.Editors;
using Umbraco.Web.Mvc;
using Yoyocms.Umbraco7.TagManager.Models;

namespace Yoyocms.Umbraco7.TagManager
{
    [PluginController("TagManager")]
    public class TagManagerAPIController : UmbracoAuthorizedJsonController
    {
        public cmsTags GetTagById(int tagId)
        {
            cmsTags tag = new cmsTags();
            var db = UmbracoContext.Application.DatabaseContext.Database;
            var query = new Sql().Select("id, tag, count(tagId) as noTaggedNodes").From("cmsTags").LeftJoin("cmsTagRelationship").On("cmsTags.id = cmsTagRelationship.tagId").Where<cmsTags>(x => x.id == tagId).GroupBy("tag, id");

            tag = db.Fetch<cmsTags>(query).FirstOrDefault();

            List<TaggedDocument> taggedDocs = GetTaggedDocumentNodeIds(tagId);

            tag.taggedDocuments = taggedDocs;

            List<TaggedMedia> taggedMedia = GetTaggedMediaNodeIds(tagId);

            tag.taggedMedia = taggedMedia;

            TagInGroup tagsInGroup = GetAllTagsInGroup(tagId);

            tag.tagsInGroup = tagsInGroup;

            return tag;
        }

        public IEnumerable<TagGroup> GetTagGroups()
        {
            var db = UmbracoContext.Application.DatabaseContext.Database;
            var query = new Sql().Select("[group]").From("cmstags").GroupBy("[group]").OrderBy("[group]");
            return db.Fetch<TagGroup>(query);
        }

        public TagInGroup GetAllTagsInGroup(int tagId)
        {
            TagInGroup tagsInGroup = new TagInGroup();
            List<plainPair> listOfTags = new List<plainPair>();

            var db = UmbracoContext.Application.DatabaseContext.Database;
            //var query = new Sql().Select("id, tag").From("cmsTags").Where(string.Format("[group] = (SELECT [group] FROM cmsTags WHERE id = {0})", tagId)).OrderBy("tag");

            var query = new Sql().Select(string.Format("id, tag FROM cmsTags where [group] = (SELECT [group] FROM cmsTags WHERE id={0}) ORDER BY tag", tagId));

            var results = db.Fetch<plainPair>(query);

            foreach (var result in results)
            {
                plainPair t = new plainPair();
                t.id = Convert.ToInt32(result.id);
                t.tag = result.tag;

                listOfTags.Add(t);

                if (result.id == tagId)
                {
                    tagsInGroup.selectedItem = t;
                }
            }

            tagsInGroup.options = listOfTags;

            return tagsInGroup;
        }

        public IEnumerable<cmsTags> GetAllTagsInGroup(string groupName)
        {
            var db = UmbracoContext.Application.DatabaseContext.Database;
            var query = new Sql().Select("id, tag, [group], count(tagId) as noTaggedNodes").From("cmsTags").LeftJoin("cmsTagRelationship").On("cmsTags.id = cmsTagRelationship.tagId").Where(string.Format("[group] = '{0}'", groupName)).GroupBy("tag, id, [group]").OrderBy("tag");

            return db.Fetch<cmsTags>(query);
        }

        public List<TaggedDocument> GetTaggedDocumentNodeIds(int tagId)
        {
            List<TaggedDocument> docs = new List<TaggedDocument>();

            var db = UmbracoContext.Application.DatabaseContext.Database;
            var query = new Sql().Select("nodeId as DocumentId").From("cmsTagRelationship").Where(string.Format("tagID = {0}", tagId));
            var results = db.Fetch<TaggedDocument>(query);
            foreach (var result in results)
            {
                try
                {
                    var n = Umbraco.Content(result.DocumentId);
                    if (!string.IsNullOrWhiteSpace(n.Name))
                    {
                        var document = new TaggedDocument { DocumentId = result.DocumentId, DocumentName = n.Name, DocumentURL = string.Format("/umbraco/#/content/content/edit/{0}", result.DocumentId.ToString()) };
                        docs.Add(document);
                    }
                }
                catch { }
            }
            return docs;
        }

        public List<TaggedMedia> GetTaggedMediaNodeIds(int tagId)
        {

            List<TaggedMedia> meds = new List<TaggedMedia>();

            var db = UmbracoContext.Application.DatabaseContext.Database;
            var query = new Sql().Select("nodeId as DocumentId").From("cmsTagRelationship").Where(string.Format("tagID = {0}", tagId));
            var results = db.Fetch<TaggedDocument>(query);
            foreach (var result in results)
            {
                try
                {
                    var n = Umbraco.Media(result.DocumentId);
                    if (!string.IsNullOrWhiteSpace(n.Name))
                    {
                        var media = new TaggedMedia { DocumentId = result.DocumentId, DocumentName = n.Text, DocumentURL = string.Format("/umbraco/#/media/media/edit/{0}", result.DocumentId.ToString()) };
                        meds.Add(media);
                    }
                }
                catch
                {
                }
            }

            return meds;
        }

        public int MoveTaggedNodes(int currentTagId, int newTagId)
        {
            var db = UmbracoContext.Application.DatabaseContext.Database;

            int success = db.Execute("Update cmsTagRelationship SET tagID = @1 WHERE tagID = @0 AND nodeId NOT IN (SELECT nodeId FROM cmsTagRelationship WHERE tagId = @1);", currentTagId, newTagId);

            if (success == 1)
            {
                success = db.Execute("DELETE FROM cmsTagRelationship WHERE tagId = @0;", currentTagId);
            }

            return success;
        }

        public int Save(cmsTags tag)
        {
            var db = UmbracoContext.Application.DatabaseContext.Database;

            int success = db.Execute("Update cmsTags set tag = @0 where id = @1", tag.tag, tag.id);

            if (success == 1 && tag.id != tag.tagsInGroup.selectedItem.id)
            {
                // Merge tags
                string sqlQuery1 = string.Format("Update cmsTagRelationship SET tagID = {0} WHERE tagID = {1} AND nodeId NOT IN (SELECT nodeId FROM cmsTagRelationship WHERE tagId = {0});", tag.tagsInGroup.selectedItem.id, tag.id);

                success = db.Execute(sqlQuery1);

                // Delete tag
                string sqlQuery2 = string.Format("DELETE FROM cmsTagRelationship WHERE tagId = {0};", tag.id);
                db.Execute(sqlQuery2);
            }

            return success;
        }

        public int AddDbEntry(string sqlStatement)
        {
            var db = UmbracoContext.Application.DatabaseContext.Database;
            var query = new Sql(sqlStatement);
            return db.Execute(query);
        }

        public int checkUserTable()
        {
            var db = UmbracoContext.Application.DatabaseContext.Database;
            string sql = "SELECT * FROM umbracoUser2App WHERE [user] = 0 and [app] = 'TagManager';";
            int success = db.Execute(sql);
            return success;
        }

        [System.Web.Http.HttpPost]
        [AcceptVerbs("POST", "GET")]
        public int DeleteTag(cmsTags tag)
        {
            var db = UmbracoContext.Application.DatabaseContext.Database;
            string sqlQuery1 = string.Format("DELETE FROM cmsTagRelationship WHERE tagId = {0};", tag.id);
            db.Execute(sqlQuery1);

            string sqlQuery2 = string.Format("DELETE FROM cmsTags WHERE id = {0};", tag.id);
            int success = db.Execute(sqlQuery2);

            return success;
        }
    }
}