﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Yoyocms.Umbraco7.TagManager.Models
{
    [DataContract(Name = "tagInGroup", Namespace = "")]
    public class TagInGroup
    {
        [DataMember(Name = "selectedItem")]
        public plainPair selectedItem { get; set; }

        [DataMember(Name = "options")]
        public List<plainPair> options { get; set; }
    }
}